package postgre

import (
	"errors"
)

type Config struct {
	Host string
	Port string
	Name string
	User string
	Pass string
}

var (
	configLink = ""
	pgIns      Postgre
)

func Init(cnf *Config) (*Postgre, error) {
	if configLink == "" {
		err := SetConfigLink(cnf)
		if err != nil {
			return nil, err
		}
	}

	if pgIns.Pool == nil {
		pgIns = Postgre{}
		if err := pgIns.connect(configLink); err != nil {
			return nil, err
		}
	}

	return &pgIns, nil
}

func SetConfigLink(cnf *Config) error {
	if err := checkConfig(cnf.Host, cnf.Port, cnf.Name, cnf.User, cnf.Pass); err != nil {
		return err
	}

	configLink = "postgres://" + cnf.User + ":" + cnf.Pass + "@" + cnf.Host + ":" + cnf.Port + "/" + cnf.Name
	return nil
}

func checkConfig(host, port, baseName, user, pass string) error {
	if host == "" {
		return errors.New("Cannot initialize connection to Postgre, host not set.  database.host")
	}

	if port == "" {
		return errors.New("Cannot initialize connection to Postgre, port not set.  database.port")
	}

	if baseName == "" {
		return errors.New("Cannot initialize connection to Postgre, base-name not set.  database.name")
	}

	if user == "" {
		return errors.New("Cannot initialize connection to Postgre, user not set.  database.user")
	}

	if pass == "" {
		return errors.New("Cannot initialize connection to Postgre, pass not set.  database.pass")
	}

	return nil
}

func RunMigration() (int, error) {
	if pgIns.Pool != nil {
		return pgIns.runMigrate()
	} else {
		return 0, errors.New("Cannot run migrate, don't initialize connection to Postgre. ")
	}
}
